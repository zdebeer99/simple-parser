﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using codesticks.parser.Internal;

namespace codesticks.parser
{

    public class SimpleParser
    {
        public static ParsedElement Parse(string Expression)
        {
            var scanner = new Scanner();
            var scanned = scanner.Scan(Expression);
            var tparse = new TokenParser();
            return tparse.Parse(scanned);
        }
    }

    //public class Parser
    //{
    //    EvalFunctionRepository mfunctions;

    //    public char[] Qoutes = new char[] { '"', '\'' };
    //    public char[] Delimiters = new char[] {',', ';',':' ,' ' };
    //    public char[] BracketOpen = new char[] { '(', '{', '['};
    //    public char[] BracketClose = new char[] { ')', '}', ']' };
    //    public char[] Operators = new char[] { '+', '-', '/', '*'};
    //    public char[] Comparer = new char[] { '=', '<', '>', '!', '&', '|', '?' };
    //    public char[] Special = new char[] { '^' };
    //    public char[] ValidName = new char[] { '_', '$', '#', '@' };

    //    public Parser()
    //    {
    //        mfunctions = new EvalFunctionRepository();
    //    }

    //    public EvalFunctionRepository FunctionRepository { get { return mfunctions; } set { mfunctions = value; } }

    //    public ParsedExpression Parse(string expression)
    //    {
    //        var scanner = new Scanner(this);
    //        var scanned = scanner.Scan(expression);
    //        var tparse = new TokenParser(this);
    //        return tparse.Parse(scanned);
    //    }        

    //    public Object Eval(string expression)
    //    {
    //        Evaluator evo = new Evaluator();
    //        evo.Functions = FunctionRepository;
    //        return evo.Eval(Parse(expression));
    //    }

    //    public Object Eval(string expression,IDictionary<string,object> variables)
    //    {
    //        Evaluator evo = new Evaluator();
    //        evo.Functions = FunctionRepository;
    //        evo.Variables = variables;
    //        return evo.Eval(Parse(expression));
    //    }

    //    internal bool IsQoute(char c)
    //    {
    //        return Qoutes.Contains(c);
    //    }

    //    internal bool IsDelimiter(char c)
    //    {
    //        return Delimiters.Contains(c);
    //    }

    //    internal bool IsOperator(char c)
    //    {
    //        return Operators.Contains(c);
    //    }

    //    internal bool IsEquater(char c)
    //    {
    //        return Comparer.Contains(c);
    //    }

    //    internal bool IsSpecial(char c)
    //    {
    //        return Special.Contains(c);
    //    }

    //    internal bool IsBracket(char c)
    //    {
    //        return IsBracketOpen(c) || IsBracketClose(c);
    //    }

    //    internal bool IsBracketOpen(char c)
    //    {
    //        return BracketOpen.Contains(c);
    //    }

    //    internal bool IsBracketClose(char c)
    //    {
    //        return BracketClose.Contains(c);
    //    }

    //    internal bool IsValidName(char c)
    //    {
    //        return Char.IsLetterOrDigit(c) || ValidName.Contains(c);
    //    }
    //}

}
