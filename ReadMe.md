Simple Parser
================
The simple parser is a experiment on writing a quick parser.

Feutures
---------
Currently the parser supports

* Math Functions [+,-,*,/] Example: (100-20)/100
* Variables Example: x=5;x*2
* Boolean Operations [==,!=,>,<,<=,>=,&&,||,?,if,!,true,false] Example: 5>2
* Function calls. sum(x)

Help
-------

**Functions**  
any word followed by '(' is marked as an function.

Example 1:  
    dosomething()

Example 2:  
    dosomething(2,3)

**Delimiters**  
';' Root delimiter. used to delimited multiple expressions.  
',' Argument Delimiter. 
':' Key Value pairs. the ':' is actually defined as a function to create key value pairs.

**Arrays**  
For Arrays you can use [] and list items inside of the brackets.

Example 1:  
[1,2,3,4]

**Dictionary**  
Use '{' '}' brackets to specify a dictionary, dictionary key valuepairs are separated by ':' and items is separeted by ','

Example 1:  
{"a":1,"b":2}

Example 2:  
{5:'a',8:'b'}


**Expressions**  
Normal math operators is supported.

Example 1:  
1+2*5

Example 1:  
(1+2)*5


**Boolean Oparators**  
All Boolean Operators is supported.

* '==', '!=' Equals, Not Equals
* '>','<','<=','>=' Larger than and smaller than.
* '!' Not operator put infront of boolean value. Example 1: !(5 < x) or !x where x is either true or false.

**If Operator**  
Use 'condition?true:false'

Or

if(condition,true,false)

Example 1:  
5<10?"Smaller":"Larger"

Example 1:  
if(5<10,"Smaller","Larger")


**Set Operator**  
Set a variable value.

Example 1:  
x=5
