﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using codesticks.parser.Internal;
using System.Collections;

namespace codesticks.parser
{
    class Program
    {
        public static ConsoleColor color = Console.ForegroundColor;
        static List<string> testcmd;
        static SimpleEvaluator sEval;

        static void Main(string[] args)
        {
            string cmd = "Sum(Add(\"filter1,filter1\",\"filter2,filter2\"),\"group1,group1\")";

            help();
            sEval = new SimpleEvaluator();
            sEval.RegisterFunctions(new TestFunctions());

            int index = 0;
            testcmd = testcommands();
            while (cmd != "q")
            {
                Console.WriteLine("Parse ('?' for help):");
                cmd = Console.ReadLine();
                try
                {
                    if (cmd == "t")
                    {
                        test();
                        continue;
                    }
                    if (cmd == "l")
                    {
                        list();
                        continue;
                    }
                    if (cmd == "?")
                    {
                        help();
                        continue;
                    }
                    if (cmd.StartsWith("r") && Int32.TryParse(cmd.Substring(1), out index))
                    {
                        test(index);
                        continue;
                    }

                    var expr = SimpleParser.Parse(cmd);
                    Console.WriteLine("Expression: {0}", expr);
                    Console.WriteLine("=> {0}", DumpObject(sEval.Eval(expr)));
                }
                catch (Exception ex)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(ex.Message);
                    //Console.WriteLine(ex.ToString());
                    Console.ForegroundColor = color;
                }

            }
        }

        static string DumpObject(object obj)
        {
            StringBuilder r = new StringBuilder();

            if (obj == null)
                return "null";

            if (obj is string)
                return obj.ToString();

            if (obj is IDictionary)
            {
                r.Append("Dictionary{");
                foreach (object item in (IEnumerable)obj)
                {
                    r.Append(DumpObject(item) + ",");
                }
                r.Remove(r.Length - 1, 1);
                r.Append("}");
                return r.ToString();
            }

            if (obj is IEnumerable)
            {
                r.Append("Array[");
                foreach (object item in (IEnumerable)obj)
                {
                    r.Append(DumpObject(item)+",");
                }
                r.Remove(r.Length - 1, 1);
                r.Append("]");
                return r.ToString();
            }
            r.Append(obj);
            return r.ToString();
        }

        static void help()
        {
            Console.WriteLine("Help");
            Console.WriteLine("q - quit");
            Console.WriteLine("t - test");
            Console.WriteLine("l - list");
            Console.WriteLine("r# - any number starts with a 'r' runs the corresponding test item. Example: r1 will eval command '1'");
            Console.WriteLine("? - help");
            Console.WriteLine("");
            Console.WriteLine("any other expression will be persed");
        }

        static void list()
        {
            for (int i = 0; i < testcmd.Count; i++)
            {
                list(i);
            }
        }

        static void list(int index)
        {
            Console.WriteLine("{0}. {1}", index, testcmd[index]);
        }

        static void test()
        {
            for (int i = 0; i < testcmd.Count; i++)
            {
                test(i);
            }
        }

        static void test(int index)
        {
            try
            {
                Console.WriteLine("{0}. {1}", index, testcmd[index]);
                var expr = SimpleParser.Parse(testcmd[index]);                
                Console.WriteLine(expr);
                Console.WriteLine("=>{0}", DumpObject(sEval.Eval(expr)));
                Console.WriteLine();
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                //Console.WriteLine(ex.ToString());
                Console.ForegroundColor = color;
            }
        }

        static List<string> testcommands()
        {
            List<string> cmd = new List<string>() {
            "10",            
            "-10",
            "-10+5",
            "1+1",
            "x=1+2;x",
            "(5+2)*10",
            "valid(5/5*10+7+8*10/2,57)",
            "valid(1+5+5*2*3+5*10,86)",
            "valid(2*3+1,7)",
            "valid(10-14/2,3)",
            "valid(4+5/5*6,10)",
            "valid(2+42-4*3,32)",
            "valid(4*(2+(1+8)+9),80)",
            "valid(2+(2*6),14)",
            "4+5/5*6/Times10((5+2)*30)*10",
            "2+(2*6)",
            "4+5/5*6",
            "5/5*10+7+8*10/2",
            "Random()",
            "Random()/100",
            "var1=5;var1",
            "$var=5;$var",
            "@var_23=5;@var_23",
            "x_y=10;x=2;valid(5*10+Times10(6/2)*x_y/x,200)", // 400
            "1,2,3,4,5",
            "[1,2,3,4,5]",
            "[1:'a',2:'b',3:'c',4:'d',5:'f']",
            "{1:'a',2:'b',3:'c',4:'d',5:'f'}",
            "'a,b,c'",
            "r=Random();Times10(r)",
            "SayHello()",
            "SayHello()=='Hello'",
            "'a'<'z'",
            "'a'>'z'",
            "date('1 jan 2013')<date('2 jan 2013')",
            "1==2",
            "x=3;valid(x>=2,true)",
            "x=2;x!=2",
            "z=1;3456!=z",
            "valid(!(5>2),false)",
            "1+1;nothing();",
            "x=6",
            "if(x>2,'x is larger','x is smaller')",
            "x>2?'x is larger':'x is smaller'",
            "'hello ''world'''",
            "''",
            "'hello \"world\"'"
            };
            return cmd;
        }

    }

    public class TestFunctions
    {
        public string SayHello()
        {
            return "Hello";
        }

        public int Random()
        {
            var r = new Random();
            return r.Next(100);
        }

        public int Times10(int arg)
        {
            return arg * 10;
        }

        public void nothing()
        {
            Console.WriteLine("Did Nothing.");
        }

        public object valid(object expression, object answer)
        {
            if (object.Equals(expression, answer))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Valid! {0}", expression);
                Console.ForegroundColor = Program.color;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Invalid result! {0} expected {1}", expression, answer);
                Console.ForegroundColor = Program.color;
            }
            return expression;
        }

        public DateTime date(string date)
        {
            return DateTime.Parse(date);
        }
    }
}
