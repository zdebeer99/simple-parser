﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using codesticks.parser.Internal;
using System.Reflection;

namespace codesticks.parser
{
    public class SimpleEvaluator
    {
        public SimpleEvaluator()
        {
            Functions = new EvalFunctionRepository();
        }

        public EvalFunctionRepository Functions
        {
            get;
            set;
        }

        public IDictionary<string, object> Variables { get; set; }

        public object Eval(string expression, IDictionary<string,object> parameters=null)
        {
            var scanned = (new Scanner()).Scan(expression);
            return Eval((new TokenParser()).Parse(scanned),parameters);
        }

        public object Eval(ParsedElement element, IDictionary<string, object> parameters = null)
        {
            var evalutor = new ElementEvaluator();
            evalutor.Variables = mergeParameters(parameters);
            evalutor.Functions = Functions;
            var r = evalutor.Eval(element);
            Variables = mergeParameters(evalutor.Variables); //Merge Variables Set in the expression back to this instance.
            return r;
        }

        private IDictionary<string, object> mergeParameters(IDictionary<string, object> parameters = null)
        {
            Dictionary<string, object> merged = new Dictionary<string, object>();
            if (Variables != null)
            {
                foreach (var item in Variables)
                    merged.Add(item.Key, item.Value);
            }
            if (parameters != null)
            {
                foreach (var item in parameters)
                {
                    if (merged.ContainsKey(item.Key))
                        merged[item.Key] = item.Value;
                    else
                        merged.Add(item.Key, item.Value);
                }
            }
            return merged;
        }

        public void SetVariable(string name, object value)
        {
            if (Variables == null)
                Variables = new Dictionary<string, object>();
            if (Variables.ContainsKey(name))
                Variables[name] = value;
            else
                Variables.Add(name, value);
        }

        public void ClearVariables()
        {
            Variables = null;
        }

        public void RegisterFunctions(object functionclass)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.RegisterFunctions(functionclass);
        }

        public void RegisterFunctions(Type functionclass)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.RegisterFunctions(functionclass);

        }

        public void RegisterFunction(string functionname, object functionclass)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.RegisterFunction(functionname,functionclass);

        }

        public void RegisterFunction(string functionname, Type functionclass)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.RegisterFunction(functionname, functionclass);
        }

        public void RegisterFunction(string AsName, string functionname, object functionclass)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.RegisterFunction(AsName, functionname, functionclass);

        }

        public void RegisterFunction(string AsName, string functionname, Type functionclass)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.RegisterFunction(AsName, functionname, functionclass);

        }

        public void RegisterFunction(MethodInfo info, object instance)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.RegisterFunction(info,instance);

        }

        public void RegisterFunction(string AsName, MethodInfo info, object instance)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.RegisterFunction(AsName, info, instance);

        }

        public void UnRegister(string name)
        {
            if (Functions == null)
                Functions = new EvalFunctionRepository();
            Functions.UnRegister(name);

        }

        public void ClearFunction()
        {
            Functions.Clear();
        }
    }
}
