﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using codesticks.parser.Internal;

namespace codesticks.parser
{
    /// <summary>
    /// Element Types
    /// function - Function has Arguments.
    /// Value - Has no Arguments
    /// None - type has not been assigned yet.
    /// </summary>
    public enum ElementType { Function, Text, Number, Bool, Variable, Collection, None }
    public class ParsedElement
    {
        public static string NoInstruction = "*nit";
        byte allowedcount=3;
        bool endatdelimiter = false;

        public ParsedElement()
        {
            Arguments = new List<ParsedElement>();
            ElementType = ElementType.None;
        }

        #region Properties        
        public string Command { get; set; }
        public string OpeningToken { get; set; }
        public ElementType ElementType { get; set; }
        public List<ParsedElement> Arguments { get; set; }
        public int Count { get { return Arguments.Count; } }
        public ParsedElement Parent { get; set; }    
        public ParsedElement this[int index]
        {
            get { return Arguments[index]; }
        }
        #endregion

        #region Build Functions

        public void Add(ParsedElement expr)
        {
            AddArg(expr);
        }

        public void Remove(ParsedElement expr)
        {
            Arguments.Remove(expr);
            expr.Parent = null;
        }

        public ParsedElement Add()
        {
            var el = new ParsedElement();
            AddArg(el);
            return el;
        }

        public ParsedElement Push()
        {
            var el = new ParsedElement();
            el.Parent = this.Parent;
            Parent.Arguments.Remove(this);
            el.Add(this);
            return el;
        }

        public ParsedElement PushOld(string cmd, ElementType elementType)
        {
            var el = new ParsedElement() { Command = cmd, ElementType = elementType };
            if (IsCommandEmpty)
            {
                Command = cmd;
                ElementType = elementType;
                return this;
            }
            Parent.Add(el);
            Parent.Remove(this);
            el.Add(this);            
            return el;
        }

        public ParsedElement Push(string cmd, ElementType elementType)
        {
            var el = new ParsedElement() { Command = cmd, ElementType = elementType };
            if (IsCommandEmpty)
            {
                throw new NotSupportedException();
                Command = cmd;
                ElementType = elementType;
                return this;
            }
            
            var arg = Arguments.Last();
            Arguments.Remove(arg);
            el.Add(arg);                        
            AddArg(el);

            return el;
        }

        public ParsedElement Add(string cmd, ElementType elementType)
        {
            //if (IsCommandEmpty)
            //{
            //    Command = cmd;
            //    ElementType = elementType;
            //    return this;
            //}
            var el = new ParsedElement() { Command = cmd, ElementType = elementType };
            AddArg(el);
            return el;
        }

        public ParsedElement Get(int index)
        {
            if (index >= Count)
                return null;
            return Arguments[index];
        }

        public ParsedElement Get(string cmd)
        {
            if (IsNoInstruction)
            {
                if (Count == 1)
                    return Arguments[0].Get(cmd);
                return null;
            }

            if (CommandEqual(cmd))
                return this;
            else
                return null;
        }

        public ParsedElement Take(int index)
        {
            if (index >= Count)
                return null;
            var r = Arguments[index];
            Arguments.RemoveAt(index);
            return r;
        }

        public void Insert(int index, ParsedElement element)
        {
            Arguments.Insert(index,element);
        }

        private void AddArg(ParsedElement expr)
        {
            if (!ArgumentsAllowed)
                throw new ParseException(string.Format("Cannot add Arguments to a {0} type, Expression: '{1}' Argument: {2}",ElementType, expr.Parent, expr));

            expr.Parent = this;
            Arguments.Add(expr);

        }
        #endregion

        #region Attributes
        public bool IsEmpty { get { return IsCommandEmpty && Count == 0; } }
        public bool HasArguments { get { return Count > 0; } }
        public bool IsLeaveNode { get { return Count==0 && !IsCommandEmpty; } }
        public bool IsCommandEmpty { get { return string.IsNullOrEmpty(Command) && string.IsNullOrEmpty(OpeningToken); } }
        public bool IsGrouped { get { return !string.IsNullOrEmpty(OpeningToken); } }
        public bool IsNoInstruction { get { return string.Equals(Command, ParsedElement.NoInstruction); } }
        public void SetNoInstruction()
        {
            Command = ParsedElement.NoInstruction;
            ElementType = ElementType.None;
        }
        public bool CommandEqual(string cmd)
        {
            if (IsNoInstruction)
            {
                if (Count == 1)
                    return Arguments[0].CommandEqual(cmd);
                return false;
            }                
            return string.Equals(Command, cmd);
        }
        public bool ArgumentsAllowed { get {
            return ElementType == ElementType.Function || ElementType==ElementType.None || allowedcount==0;
        } }
        #endregion

        public override string ToString()
        {
            if (IsCommandEmpty)
                return "null";
            string disp = IsNoInstruction ? "" : Command;
            if (Count > 0 && (IsNoInstruction))
                return string.Format("{1}", disp, string.Join(";", Arguments.Select(a => string.Format("{0}", a.ToString()))));
            if (Count > 0 && (IsGrouped))
                return string.Format("{0}({1})", disp, string.Join(",", Arguments.Select(a => string.Format("{0}", a.ToString()))));
            if (Count>0)
                return string.Format(" {0}({1})", disp, string.Join(",", Arguments.Select(a => string.Format("{0}", a.ToString()))));
            if (Count==0)
                return string.Format("{0}", disp);
            return string.Format("Err1:{0}", Command);
        }

        internal void EndAtDelimiter()
        {
            allowedcount = 3;
            endatdelimiter = true;
        }

        internal void EndAtCustom()
        {
            allowedcount = 3;
            endatdelimiter = false;
        }


        internal void NoArguments()
        {
            allowedcount = 0;
            endatdelimiter = false;
        }

        internal void SingleArgument()
        {
            allowedcount = 1;
            endatdelimiter = false;
        }

        internal void DoubleArgument()
        {
            allowedcount = 2;
            endatdelimiter = false;
        }

        internal bool IsAtEnd()
        {
            if (endatdelimiter)
                return false;
            if (allowedcount > 2)
                return false;
            return Count == allowedcount;
        }

        internal bool IsAtEnd(Token t)
        {
            if (endatdelimiter)
                return t.IsDelimiter();
            return false;
        }



    }
}
