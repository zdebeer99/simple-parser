﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace codesticks.parser.Internal
{
    public enum tokentype { text, number, delimiter, rfunction, lrfunction, mathoperator, equator, special, none,open,close }

    public class Token
    {
        public string text; public tokentype mode;

        public static char[] Qoutes = new char[] { '"', '\'' };
        public static char[] Delimiters = new char[] { ',', ';', ' ' };
        public static char[] BracketOpen = new char[] { '(', '{', '[' };
        public static char[] BracketClose = new char[] { ')', '}', ']' };
        public static char[] MathFunction = new char[] { '+', '-', '/', '*' };
        public static char[] EqualsFunction = new char[] { '=', '<', '>', '&', '|','!' };
        public static char[] LRFunc = new char[] { '?',':' };
        public static char[] RFunc = new char[] {  };
        public static char[] ValidName = new char[] { '_', '$', '#', '@' };

        public override bool Equals(object obj)
        {
            return string.Equals(text, obj);
        }

        public override int GetHashCode()
        {
            if (string.IsNullOrEmpty(text))
                return 0;
            return text.GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("{1}:'{0}' ", text, mode);
        }

        #region Checks
        char getchar()
        {
            if (string.IsNullOrEmpty(text))
                return (char)0;
            return text[0];
        }
        static char getchar(string command)
        {
            if (string.IsNullOrEmpty(command))
                return (char)0;
            return command[0];
        }
        public bool IsQoute()
        {
                char c = getchar();
                return IsQoute(c);
        }

        public bool IsDelimiter()
        {
            char c = getchar();
            return IsDelimiter(c);
        }

        public bool IsRFunc()
        {
            char c = getchar();
            return IsRFunc(c);
        }

        public bool IsLRFunc()
        {
            char c = getchar();
            return IsLRFunc(c);
        }

        public bool IsMathFunc()
        {
            char c = getchar();
            return IsMathFunc(c);
        }

        public bool IsBracket()
        {
            return IsBracketOpen() || IsBracketClose();
        }

        public bool IsBracketOpen()
        {
            char c = getchar();
            return IsBracketOpen(c);
        }

        public bool IsBracketClose()
        {
            char c = getchar();
            return IsBracketClose(c);
        }

        public bool IsValidName()
        {
            if (string.IsNullOrEmpty(text))
                return false;
            foreach (char c in text)
            {
                if (!(Char.IsLetterOrDigit(c) || IsValidName(c)))
                    return false;                    
            }
            return true;
        }

        public static bool IsQoute(string command)
        {
            char c = getchar(command);
            return IsQoute(c);
        }

        public static bool IsDelimiter(string command)
        {
            char c = getchar(command);
            return IsDelimiter(c);
        }

        public static bool IsRFunc(string command)
        {
            char c = getchar(command);
            return IsRFunc(c);
        }

        public static bool IsLRFunc(string command)
        {
            char c = getchar(command);
            return IsLRFunc(c);
        }

        public static bool IsMathFunc(string command)
        {
            char c = getchar(command);
            return IsMathFunc(c);
        }

        public static bool IsBracket(string command)
        {
            return IsBracketOpen(command) || IsBracketClose(command);
        }

        public static bool IsBracketOpen(string command)
        {
            char c = getchar(command);
            return IsBracketOpen(c);
        }

        public static bool IsBracketClose(string command)
        {
            char c = getchar(command);
            return IsBracketClose(c);
        }

        public static bool IsValidName(string command)
        {
            if (string.IsNullOrEmpty(command))
                return false;
            foreach (char c in command)
            {
                if (!(Char.IsLetterOrDigit(c) || IsValidName(c)))
                    return false;
            }
            return true;
        }

        public static bool IsLRFunc(char c)
        {
            return LRFunc.Contains(c) || IsMathFunc(c) || IsEqualsFunc(c);
        }

        public static bool IsRFunc(char c)
        {
            return RFunc.Contains(c);
        }

        public static bool IsQoute(char c)
        {
            return Qoutes.Contains(c);
        }

        public static bool IsDelimiter(char c)
        {
            return Delimiters.Contains(c);
        }

        public static bool IsMathFunc(char c)
        {
            return MathFunction.Contains(c);
        }

        public static bool IsEqualsFunc(char c)
        {
            return EqualsFunction.Contains(c);
        }

        public static bool IsBracket(char c)
        {
            return IsBracketOpen(c) || IsBracketClose(c);
        }

        public static bool IsBracketOpen(char c)
        {
            return BracketOpen.Contains(c);
        }

        public static bool IsBracketClose(char c)
        {
            return BracketClose.Contains(c);
        }

        public static bool IsValidName(char c)
        {
            return Char.IsLetterOrDigit(c) || ValidName.Contains(c);
        }

        public static bool IsNumber(char c)
        {
            return Char.IsNumber(c) || c.Equals('.');
        }

        public static char GetOpening(string closing)
        {
            if (!BracketClose.Contains(closing[0]))
                throw new ParseException("Invalid Closing bracket '"+closing+"', bracket not defined.");

            int i = IndexOf(BracketClose, closing[0]);
            if (i < 0)
                throw new ParseException("Bracket '"+closing+"' not defined.");
            return BracketOpen[i];
        }

        public static char GetClosing(string opening)
        {
            if (!BracketOpen.Contains(opening[0]))
                throw new ParseException("Invalid Closing bracket '" + opening + "', bracket not defined.");

            int i = IndexOf(BracketOpen, opening[0]);
            if (i < 0)
                throw new ParseException("Bracket '" + opening + "' not defined.");
            return BracketClose[i];
        }

        public static int IndexOf(char[] array, char c)
        {
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == c)
                    return i;
            }
            return -1;
        }
        #endregion
    }
}
