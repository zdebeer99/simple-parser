﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace codesticks.parser.Internal
{
    public class TokenList
    {
        public List<Token> Tokens { get; set; }

        public TokenList()
        {
            Tokens = new List<Token>();
        }

        public Token this[int index]
        {
            get { return Tokens[index]; }
        }

        public int Count
        {
            get { return Tokens.Count; }
        }

        public bool Add(string text, tokentype type)
        {
            if (string.IsNullOrWhiteSpace(text))
                return false;
            Tokens.Add(new Token() { text = text, mode = type });
            return true;
        }

        public bool Add(char text, tokentype type)
        {
            if (Char.IsWhiteSpace(text))
                return false;
            Tokens.Add(new Token() { text = text.ToString(), mode = type });
            return true;
        }

        public bool Add(StringBuilder text, tokentype type)
        {
            if (text == null || string.IsNullOrWhiteSpace(text.ToString()))
                return false;
            if (text != null)
                Tokens.Add(new Token() { text = text.ToString(), mode = type });
            return true;
        }

        public bool Add(Token token)
        {
            if (string.IsNullOrWhiteSpace(token.text))
                return false;
            Tokens.Add(token);
            return true;
        }

        public override string ToString()
        {
            return string.Join(" ", Tokens.Select(a => string.Format("[{0}]", a.text)));
        }
    }
}
