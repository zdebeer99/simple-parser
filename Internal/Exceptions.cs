﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace codesticks.parser.Internal
{
    [Serializable]
    public class ParseException : Exception
    {
        public ParseException() { }
        public ParseException(string message) : base(message) { }
        public ParseException(string message, Exception inner) : base(message, inner) { }
        protected ParseException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }

    [Serializable]
    public class EvaluatorException : Exception
    {
        public EvaluatorException() { }
        public EvaluatorException(string message) : base(message) { }
        public EvaluatorException(string message, Exception inner) : base(message, inner) { }
        protected EvaluatorException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
