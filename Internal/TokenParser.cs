﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace codesticks.parser.Internal
{
    public class TokenParser
    {
        TokenList mtokens;
        int index = 0;
        int bracketcount = 0;
        ParsedElement root;
        ParsedElement expr;

        public TokenParser()
        {

        }

        public ParsedElement Parse(TokenList tokens)
        {
            mtokens = tokens;
            index = 0;
            bracketcount = 0;
            root = new ParsedElement();
            root.SetNoInstruction();
            expr = new ParsedElement();
            expr.SetNoInstruction();
            root.Add(expr);

            build();
            return root;
        }

        #region expression builder
        void build()
        {
            while (go())
            {
                Token t = next();
                baseproxy(t);
            }

            if (bracketcount != 0)
                throw ex("Invalid Bracket Count! make sure each opning bracket has a closing bracket.");
        }

        void baseproxy(Token t)
        {
            upwhenfull(t);

            if (HandleText(t))
                return;

            if (HandleNumber(t))
                return;

            if (HandleLRFunction(t))
                return;

            if (HandleRFunction(t))
                return;

            if (HandleOpen(t))
                return;

            if (HandleClose(t))
                return;

            if (HandleDelimiter(t))
                return;

            if (HandleNone(t))
                return;


            throw new ParseException("Un Expected Token '" + t.text + "'");
        }


        #endregion

        #region Handlers
        bool HandleText(Token t)
        {
            if (t.mode != tokentype.text)
                return false;
            expr = Add(t.text, ElementType.Text);
            expr.NoArguments();
            return true;
        }

        bool HandleNumber(Token t)
        {
            if (t.mode != tokentype.number)
                return false;
            expr = Add(t.text, ElementType.Number);
            expr.NoArguments();
            return true;
        }

        bool HandleDelimiter(Token t)
        {
            if (t.mode != tokentype.delimiter)
                return false;
            if (t.Equals(";"))
            {
                if (bracketcount > 0)
                    throw ex("Invalid end of funtion, not expecting ';'. {0}", expr.ToString());
                expr = new ParsedElement();
                expr.SetNoInstruction();
                root.Add(expr);
                return true;
            }

            if (expr.IsAtEnd(t))
            {
                up();
            }

            return true;
        }

        bool HandleRFunction(Token t)
        {
            if (t.mode != tokentype.rfunction)
                return false;

            if (t.IsRFunc())
            {
                expr = Add(t.text, ElementType.Function);
                expr.SingleArgument();
                next();
                return true;
            }

            Token n = peek();
            if (n != null && n.Equals("("))//the command is a function.
            {
                expr = Add(t.text, ElementType.Function);
                expr.EndAtCustom();
                expr.OpeningToken = n.text;
                bracketcount++;
                next();
                return true;
            }

            //check for bool types.
            if (string.Equals(t.text, "true"))
            {
                expr = Add(t.text, ElementType.Bool);
                expr.NoArguments();
                return true;
            }
            if (string.Equals(t.text, "false"))
            {
                expr = Add(t.text, ElementType.Bool);
                expr.NoArguments();
                return true;
            }

            //the command is a variable
            expr = Add(t.text, ElementType.Variable);
            expr.NoArguments();
            return true;
        }

        bool HandleLRFunction(Token t)
        {
            if (HandleMathFunction(t))
                return true;

            if (HandleBoolOperation(t))
                return true;

            if (HandleOtherLFFunction(t))
                return true;

            return false;
        }

        bool HandleMathFunction(Token t)
        {
            if (t.mode != tokentype.mathoperator)
                return false;

            var n = peek();
            if (n == null)
                throw ex("Equation cannot end with a math operator. '{0}', '{1}'", t.text,expr);
            if (!expr.HasArguments)
            {
                Add("0", ElementType.Number);
                up();
            }

            //if (expr.CommandEqual(t.text))
            //{
            //    return true;
            //}

            var prev = first();
            if (prev != null && !prev.IsNoInstruction &&  Token.IsMathFunc(prev.Command[0]))
            {
                if (oporder(t.text) < oporder(prev.Command))
                {
                    var arg = prev.Take(1);
                    expr = prev.Add(t.text, ElementType.Function);
                    expr.Add(arg);
                    expr.DoubleArgument();
                    return true;
                }
            }
            expr = expr.Push(t.text, ElementType.Function);
            expr.DoubleArgument();
            return true;
        }

        bool HandleBoolOperation(Token t)
        {
            if (t.mode != tokentype.equator)
                return false;

            var n = peek();
            if (n == null)
                throw ex("expression cannot end with '{0}'", t.text);

            if (t.Equals("!"))
            {
                expr = expr.Add("!", ElementType.Function);
                expr.SingleArgument();
                return true;
            }
            if (!expr.HasArguments)
                throw ex("parameter before '{0}' not set.", t.text);

            if (t.Equals("="))
            {
                if (single().ElementType != ElementType.Variable)
                    ex("Expecting a Variable before '='. {0}", expr);
                expr = expr.Push(t.text, ElementType.Function);
                expr.EndAtDelimiter();
                //expr = addnon();
                return true;
            }

            expr = expr.Push(t.text, ElementType.Function);
            expr.DoubleArgument();
            return true;
        }

        bool HandleOtherLFFunction(Token t)
        {
            if (t.mode != tokentype.lrfunction)
                return false;

            var n = peek();
            if (n == null)
                throw ex("expression cannot end with '{0}'", t.text);
            if (expr.Parent == null)
                throw ex("Invalid function construction at '{0}'", t.text);
            if (!expr.HasArguments)
                throw ex("parameter before '{0}' not set.", t.text);


            expr = expr.Push(t.text, ElementType.Function);
            expr.EndAtDelimiter();

            return true;
        }

        bool HandleNone(Token t)
        {
            if (t.mode != tokentype.none)
                return false;
            expr.EndAtDelimiter();
            //do stuff
            return true;
        }

        bool HandleOpen(Token t)
        {
            if (t.mode != tokentype.open)
                return false;
            expr = Add(ParsedElement.NoInstruction, ElementType.None);
            expr.EndAtCustom();
            expr.OpeningToken = t.text;
            bracketcount++;
            return true;
        }

        bool HandleClose(Token t)
        {
            if (t.mode != tokentype.close)
                return false;
            callopseToBracket();
            validateClosing(t);
            up();
            bracketcount--;
            return true;
        }

        #endregion

        #region checks

        void validateClosing(Token token)
        {
            string text = token.text;

            if (expr.Parent == null)
                throw ex("Invalid closing bracket! '{0}' no matching opening bracket.", expr);

            if (string.IsNullOrEmpty(expr.OpeningToken))
                throw ex("Invalid Closing bracket '{0}', no matching opening bracket.", expr);

            char ob = Token.GetOpening(text);
            if (expr.OpeningToken[0] != ob)
                throw ex("Opening and closing brackets does not match, Expecting: '{0}' requested: '{1}', make sure the bracket properties is set correctly. '{2}'", ob, expr.OpeningToken[0], expr);
        }

        #endregion

        #region Build Functions

        private void Add(ParsedElement expression)
        {
            expr.Add(expression);
        }

        private void Remove(ParsedElement expression)
        {
            expr.Remove(expr);
        }

        private ParsedElement Add()
        {
            expr = expr.Add();
            return expr;
        }

        private ParsedElement Push()
        {
            expr = expr.Push();
            return expr;
        }

        private ParsedElement Push(string cmd, ElementType elementType)
        {
            expr = expr.Push(cmd, elementType);
            return expr;
        }

        private ParsedElement Add(string cmd, ElementType elementType)
        {
            expr = expr.Add(cmd, elementType);
            return expr;
        }



        #endregion

        #region Token Navigation
        Token peek()
        {
            if (!go())
                return null;
            var t = mtokens[index];
            return t;
        }

        Token peek(int rel)
        {
            if (index + rel < 0 || index + rel >= mtokens.Count)
                return null;
            var t = mtokens[index + rel];
            return t;
        }

        Token next()
        {
            if (!go())
                return null;
            var t = mtokens[index];
            index++;
            return t;
        }

        bool go()
        {
            return index < mtokens.Count;
        }

        bool isend()
        {
            return !go();
        }

        void up()
        {
            expr = expr.Parent;
        }

        void upwhenfull(Token t)
        {
            if (expr.IsAtEnd())
            {
                up();
                upwhenfull(t);
            }
        }

        ParsedElement single()
        {
            if (expr.Count != 1)
                ex("Single argument required!");
            return expr[0];
        }

        ParsedElement first()
        {
            return expr.Get(0);
        }

        ParsedElement addnon()
        {
            var x = new ParsedElement();
            x.SetNoInstruction();
            Add(x);
            return x;
        }

        byte oporder(string op)
        {
            switch (op)
            {
                case "^":
                    return 0;                    
                case "*":
                    return 1;
                case "/":
                    return 1;
                case "+":
                    return 2;
                case "-":
                    return 2;
                default:
                    return 5;
            }
        }

        #endregion

        void callopseToBracket()
        {
            while (expr.Parent != null && string.IsNullOrEmpty(expr.OpeningToken))
                expr = expr.Parent;
        }

        Exception ex(string format, params object[] args)
        {
            return new ParseException(string.Format(format, args));
        }
    }
}
