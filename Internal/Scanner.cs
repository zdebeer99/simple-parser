﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace codesticks.parser.Internal
{
    public class Scanner
    {
        //Expression
        string e;
        //Location
        int loc;
        //expression length
        int len;
        TokenList tchain;

        public Scanner()
        {            
        }

        public TokenList Scan(string expression)
        {
            char c;
            e = expression;
            loc = 0;
            len = expression.Length;

            StringBuilder r = new StringBuilder();
            tchain = new TokenList();
            while (go())
            {
                c = peek();
                if (Char.IsWhiteSpace(c))
                {
                    nextchar();
                    continue;
                }

                if (Token.IsQoute(c))
                {
                    scantext();
                    continue;
                }

                if (Token.IsDelimiter(c))
                {
                    scandelimiter();
                    continue;
                }

                if (Token.IsBracketOpen(c))
                {
                    tchain.Add(c, tokentype.open);
                    nextchar();
                    continue;
                }

                if (Token.IsBracketClose(c))
                {
                    tchain.Add(c, tokentype.close);
                    nextchar();
                    continue;
                }

                if (Token.IsMathFunc(c))
                {
                    tchain.Add(c, tokentype.mathoperator);
                    nextchar();
                    continue;
                }

                if (Token.IsEqualsFunc(c))
                {
                    scanequals();
                    continue;
                }

                if (Char.IsNumber(c))
                {
                    scannumber();
                    continue;
                }

                if (Token.IsValidName(c)||Token.IsRFunc(c))
                {
                    scanrfunc();
                    continue;
                }

                if (Token.IsLRFunc(c))
                {
                    tchain.Add(c, tokentype.lrfunction);
                    nextchar();
                    continue;
                }

                throw new ParseException("Invalid Character found. '" + c.ToString() + "'");
            }
            return tchain;
        }

        bool scantext()
        {
            StringBuilder r = new StringBuilder();
            char c = nextchar();
            char startqoute = c;
            r.Append(c);
            while (go())
            {
                c = peek();
                if (c == startqoute && peek(1) == startqoute)//Escape Qoutes
                {
                    c = nextchar();
                    r.Append(c);
                    c = nextchar();
                    continue;
                }
                if (c == startqoute)
                {
                    r.Append(c);
                    nextchar();
                    return tchain.Add(r, tokentype.text);
                }
                c = nextchar();
                r.Append(c);
            }
            throw new ParseException("String not closed with qoutes!");
        }

        bool scannumber()
        {
            StringBuilder r = new StringBuilder();
            char c = nextchar();
            r.Append(c);
            while (go())
            {
                c = peek();
                if (!Char.IsNumber(c) && c != '.')
                {
                    return tchain.Add(r, tokentype.number) || tchain.Add(c, tokentype.delimiter);
                }
                c = nextchar();
                r.Append(c);
            }
            return tchain.Add(r, tokentype.number);

        }

        bool scanrfunc()
        {
            StringBuilder r = new StringBuilder();
            char c = nextchar();
            r.Append(c);
            while (go())
            {
                c = peek();
                if (!Token.IsValidName(c))
                    return tchain.Add(r, tokentype.rfunction) || tchain.Add(c, tokentype.delimiter);
                c = nextchar();
                r.Append(c);
            }
            return tchain.Add(r, tokentype.rfunction);
        }

        bool scandelimiter()
        {
            char c = nextchar();
            tchain.Add(c, tokentype.delimiter);
            while (go())
            {
                char p = peek();
                if (p == c)
                    c = nextchar();
                else
                    break;
            }
            return true;
        }

        bool scanequals()
        {
            StringBuilder r = new StringBuilder();
            while (go())
            {
                char c = nextchar();
                r.Append(c);
                c = peek();
                if (!Token.IsEqualsFunc(c))
                    break;
            }
            tchain.Add(r, tokentype.equator);
            return true;
        }

        char nextchar()
        {
            if (loc < len)
            {
                char c = e[loc];
                loc++;
                return c;
            }
            return (char)0;
        }

        char peek()
        {
            if (loc < len)
            {
                return e[loc];
            }
            return (char)0;
        }

        char peek(int rel)
        {
            if ((loc + rel) >= 0 && (loc + rel) < len)
                return e[loc + rel];
            return (char)0;
        }

        bool go()
        {
            return loc < len;
        }
    }
}
